﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SnakeGame.Clases;

namespace SnakeGame
{
    public partial class Form1 : Form
    {
        Random randFood = new Random();
        Graphics paper;
        Snake snake = new Snake();
        Food food;

        bool up = false;
        bool down = false;
        bool right = false;
        bool left = false;
        public Form1()
        {
            InitializeComponent();
            food = new Food(randFood);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down && !up)
            {
                down = true;
                up = false;
                right = false;
                left = false;
            }
            if (e.KeyData == Keys.Up && !down)
            {
                down = false;
                up = true;
                right = false;
                left = false;
            }
            if (e.KeyData == Keys.Right && !left)
            {
                down = false;
                up =false;
                right = true;
                left = false;
            }
            if (e.KeyData == Keys.Left && !right)
            {
                down = false;
                up = false;
                right = false;
                left = true;
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            paper = e.Graphics;
            food.drawFood(paper);
            snake.drawSnake(paper);



        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (down)
            {
                snake.moveDown();
            }
            if (up)
            {
                snake.moveUp();
            }
            if (right)
            {
                snake.moveRight();

            }
            if (left)
            {
                snake.moveLeft();
            }

            for (int i = 0; i < snake.snakeRec.Count - 1; i++)
            {
                if (snake.snakeRec[i].IntersectsWith(food.foodRec))
                {
                    snake.growSnake();
                    food.foodLocation(randFood);
                }
            }
            collision();
            this.Invalidate();
        }

        public void collision()
        {
            for (int i = 1; i < snake.snakeRec.Count-1; i++)
            {
                if (snake.snakeRec[0].IntersectsWith(snake.snakeRec[i]))
                {
                    timer1.Enabled = false;
                    MessageBox.Show("Perdiste");
                }
            }
            if (snake.snakeRec [0].X<0 || snake.snakeRec[0].X>400)
            {
                timer1.Enabled = false;
                MessageBox.Show("Perdiste");
            }
            if (snake.snakeRec[0].Y < 0 || snake.snakeRec[0].Y > 400)
            {
                timer1.Enabled = false;
                MessageBox.Show("Perdiste");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
