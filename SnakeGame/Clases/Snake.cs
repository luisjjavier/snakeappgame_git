﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SnakeGame.Clases
{
    public class Snake
    {
        public List<Rectangle> snakeRec;
        private SolidBrush brush;
        private int x, y, width, height;
        public Snake()
        {
            snakeRec = new List<Rectangle>();
            brush = new SolidBrush(Color.ForestGreen);
            x = 20;
            y = 0;
            width = 10;
            height=10;
            for (int i = 0; i < 3; i++)
            {
                snakeRec.Add(new Rectangle(x, y, width, height));
                x -= 10;
            }
        }
        public void drawSnake(Graphics paper)
        {
            foreach (var rec in snakeRec)
            {
                paper.FillEllipse(brush, rec);
            }
        }

        public void drawSnake() 
        {
            for (int i = snakeRec.Count - 1; i >0; i--)
            {
                snakeRec[i] = snakeRec[i - 1];
            }
        }
        public void moveDown()
        {
            drawSnake();

            snakeRec[0] = new Rectangle(snakeRec[0].X, snakeRec[0].Y + 10, width, height);
        }
        public void moveUp()
        {
            drawSnake();

            snakeRec[0] = new Rectangle(snakeRec[0].X, snakeRec[0].Y -10, width, height);
        }
        public void moveRight()
        {
            drawSnake();

            snakeRec[0] = new Rectangle(snakeRec[0].X +10, snakeRec[0].Y , width, height);
        }
        public void moveLeft()
        {
            drawSnake();

            snakeRec[0] = new Rectangle(snakeRec[0].X-10, snakeRec[0].Y, width, height);
        }
        public void growSnake()
        {
            snakeRec.Add(new Rectangle(snakeRec.Last().X,snakeRec.Last().Y,snakeRec.Last().Width,snakeRec.Last().Height));
        }
    }
}
